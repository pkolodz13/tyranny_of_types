#pragma once
#include "Utilities/Algo_Pack.h"
#include "Utilities/Helpers.h"
#include <vector>
#include <array>
#include <list>
#include<memory>
#include <type_traits>
#include <tuple>
#include <string>

namespace ser
{ 
	template<class t>
	struct named_element
	{
		named_element(t& element,const std::string & name):element(element),name(name) {}
		t& element;
		std::string name;
	};
	template<class t>
	struct Memebers_detection
	{
		template<class t2, class = decltype(declval<t2&>().collected_named_members())>
		static true_type collected_named_members_d(int);
		template<class t2>
		static false_type collected_named_members_d(...);

		static constexpr bool has_collected_named_members = decltype(collected_named_members_d<t>(1))::value;

		template<class t2, class = decltype(declval<t2&>().collected_members())>
		static true_type collected_members_d(int);
		template<class t2>
		static false_type collected_members_d(...);

		static constexpr bool has_collected_members = decltype(collected_members_d<t>(1))::value;

		template<class t2, class = decltype(declval<t2&>().collected_members_names())>
		static true_type collected_members_names_d(int);
		template<class t2>
		static false_type collected_members_names_d(...);

		static constexpr bool has_collected_members_names = decltype(collected_members_names_d<t>(1))::value;

		static constexpr bool has_named_members = has_collected_named_members || (has_collected_members&&has_collected_members_names);
		static constexpr bool has_members = has_collected_named_members || has_collected_members;


		template<class ... tt_v>
		static true_type collected_named_members_correct_type_d(std::tuple<named_element<tt_v>...> );
		template<class t2>
		static false_type collected_named_members_correct_type_d(...);

		template<class ... tt_v>
		static true_type collected_members_correct_type_d(std::tuple<tt_v & ...>);
		template<class t2>
		static false_type collected_members_correct_type_d(...);

		template<class t2,size_t I,class =enable_if_t<is_convertible_v<t2,std::string>>>
		static true_type collected_members_names_correct_type_d(std::array<t2,I>);
		template<class t2>
		static false_type collected_members_names_correct_type_d(...);

		static constexpr bool collected_named_members_is_correct()
		{
			if constexpr (has_collected_named_members)
			{
				return decltype(collected_named_members_correct_type_d(declval<t&>().collected_named_members()))::value;
			}
			return false;
		}
		static constexpr bool collected_members_is_correct()
		{
			if constexpr (has_collected_members)
			{
				return decltype(collected_members_correct_type_d(declval<t&>().collected_members()))::value;
			}
			return false;
		}
		static constexpr bool collected_members_names_is_correct()
		{
			if constexpr (has_collected_members_names)
			{
				return decltype(collected_members_names_correct_type_d(declval<t&>().collected_members_names()))::value;
			}
			return false;
		}
	};
	template<class t,class = enable_if_t<Memebers_detection<t>::has_named_members>>
	struct Collected_named_members
	{
		template<size_t ... I>
		static auto from_members_and_names_to_named(t & object,index_sequence<I...>)
		{
			auto members = object.collected_members();
			auto names = object.collected_members_names();
			return make_tuple(named_element(std::get<I>(members), std::get<I>(names))...);
		}
		using detection = Memebers_detection<t>;
		static auto collected_named_members(t & object)
		{
			if constexpr (detection::has_collected_named_members)
			{
				static_assert(detection::collected_named_members_is_correct(),"collected_named_members has bad type");
				return object.collected_named_members();
			}
			else
			{
				static_assert(detection::collected_members_is_correct(), "collected_members has bad type");
				static_assert(detection::collected_members_names_is_correct(), "collected_members_names has bad type");
				using members_collection_tuple = decltype(object.collected_members());
				return from_members_and_names_to_named(object, make_index_sequence<std::tuple_size_v<members_collection_tuple>>{});
			}
		}
	};
	template<class t, class = enable_if_t<Memebers_detection<t>::has_members>>
	struct Collected_members
	{
		template<size_t ... I>
		static auto from_named_members_to_members(t & object, index_sequence<I...>)
		{
			auto members = object.collected_named_members();
			return tie(std::get<I>(members).element...);
		}
		using detection = Memebers_detection<t>;
		static auto collected_named_members(t & object)
		{
			if constexpr (detection::has_collected_members)
			{
				static_assert(detection::collected_members_is_correct(), "collected_members has bad type");
				return object.collected_members();
			}
			else
			{
				static_assert(detection::collected_named_members_is_correct(), "collected_named_members has bad type");
				using members_collection_tuple = decltype(object.collected_named_members());
				return from_named_members_to_members(object, make_index_sequence<std::tuple_size_v<members_collection_tuple>>{});
			}
		}
	};

	template<class t, class Enable = void>
	struct Members_pointers;
	template<class t>
	struct Members_pointers<t, decltype(declval<t>().members_pointers(), void())>
	{
		static auto members_pointers(t & object)
		{
			return object.members_pointers();
		}
	};

	template<class t, class Enable = void>
	struct Members_names;
	template<class t>
	struct Members_names<t, decltype(declval<t>().members_names(), void())>
	{
		static auto members_names(t & object)
		{
			return object.members_names();
		}
	};


	template<class t,class Enable1 = void,class Enable2 = void,class Enable3 = void>
	struct Internal_broker_type : std::false_type { };
	template<typename t>
	struct Internal_broker_type<t, decltype(declval<typename t::broker_type>(), void()),
		decltype(declval<t&>() = declval<typename t::broker_type>(), void()),
		decltype(declval<typename t::broker_type &>() = declval<t>(), void())> : std::true_type { };

	template<class t, class Enable = void>
	struct Broker;
	template<class t>
	struct Broker<t, enable_if_t<Internal_broker_type<t>::value>>
	{
		using convert_type = typename t::broker_type;
		static void broker_to_object(const convert_type & from,t & to)
		{
			to = from;
		}
		static void object_to_broker( const t & from, convert_type & to)
		{
			to = from;
		}
	};

	template<class t, class Enable1 = void, class Enable2 = void, class Enable3 = void, class Enable4 = void>
	struct Is_valid_varianter :std::false_type {};
	template<class t>
	struct Is_valid_varianter<t, 
		decltype(declval<vector<string>>() = t::variants_names()  , void()),
		decltype(declval<typename t::variants_p>(), void()),
		decltype(t::get_ensured(declval<t&>()), void()),
		decltype(t::current_index(declval<t&>()), void())
	> : std::true_type {};

}
