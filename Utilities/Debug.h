#pragma once
#include <cassert>
#include <chrono>
#include <ctime>
#include <iostream>
#include "types.h"
#include "Helpers.h"

template<class t>
struct single_instance :non_replicable
{
#if  _DEBUG
private:
	inline static single_instance<t> * current{ nullptr };
public:
	single_instance()
	{
		assert(current == nullptr);
		current = this;
	}
	~single_instance()
	{
		assert(current == this);
		current = nullptr;
	}
#endif 
};
template<class t>
struct indexed_instance :non_replicable
{
	size_t unit;
#if  _DEBUG
	inline static std::vector<indexed_instance<t>*> current;
	indexed_instance(size_t  unit) :unit(unit)
	{
		if (unit >= current.size())
		{
			current.resize(unit + 1, nullptr);
		}
		assert(current[unit] == nullptr);
		current[unit] = this;
	}
	~indexed_instance()
	{
		assert(current[unit] == this);
		current[unit] = nullptr;

	}
#else
	indexed_instance(size_t unit) :unit(unit)
	{
	}
#endif 
};
template<class t>
struct book_instance
{
#if  _DEBUG
	inline static size_t readers = 0;
	inline static size_t writers = 0;

	struct reader
	{
		reader()
		{
			assert(writers == 0);
			readers++;
		}
		~reader()
		{
			readers--;
		}
	};
	struct writer
	{
		writer()
		{
			assert(writers == 0);
			assert(readers == 0);
			writers++;
		}
		~writer()
		{
			writers--;
		}
	};
#else
	struct reader
	{};
	struct writer
	{};
#endif
};

template<class t = std::chrono::milliseconds>
auto current_time()
{
	return  std::chrono::duration_cast<t>(
		std::chrono::system_clock::now().time_since_epoch()).count();
}
template<class t = std::chrono::milliseconds>
auto last_time()
{
	static auto last = current_time();
	auto duration = current_time() - last;
	last = current_time();
	if (duration > 100)
		auto a = 100;
	return  duration;
}