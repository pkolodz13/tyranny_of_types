#pragma once
#include "types.h"
#include "Helpers.h"


template<class t>
struct Loose_Element;
template<class t>
struct Loose_Node:non_replicable
{
	using type = t;
	using Node = Loose_Element<t>;

	Loose_Node<t> * back;
	Loose_Node<t> * next;

private:
	inline void connect_next(Loose_Node * node)
	{
		this->next = node;
		node->back = this;
	}
	inline void connect_self()
	{
		back = this;
		next = this;
	}
public:

	Loose_Node()
	{
		connect_self();
	}
	~Loose_Node()
	{
		back->connect_next(next);
	}
	void detach()
	{
		back->connect_next(next);
		connect_self();
	}
	void step_foreward()
	{
		Loose_Node * initial_next = next;
		Loose_Node * initial_back = back;
		Loose_Node * initial_second = initial_next->next;

		initial_back->connect_next(initial_next);
		initial_next->connect_next(this);
		this->connect_next(initial_second);
	}
	void AttachListBefore(Loose_Node & n)//chain
	{
		Loose_Node * last_of_this = this->back;
		Loose_Node * last_of_n = n.back;

		last_of_this->connect_next(&n);
		last_of_n->connect_next(this);
	}
	void AttachListAfter(Loose_Node & n)//chain
	{
		Loose_Node * first_of_this = this->next;
		Loose_Node * last_of_n = n.back;

		this->connect_next(&n);
		last_of_n->connect_next(first_of_this);
	}

	bool single()
	{
		return next == this;
	}

	std::shared_ptr<Loose_Element<t> > AddFirst(typename OwnersipOrganizer<t>::copying_type e)
	{
		std::shared_ptr<Loose_Element<t>> nowy(new Loose_Element<t>(e));
		AttachListAfter(*nowy);
		return nowy;
	}
	std::shared_ptr<Loose_Element<t> > AddLast(typename OwnersipOrganizer<t>::copying_type e)
	{
		std::shared_ptr<Loose_Element<t>> nowy(new Loose_Element<t>(e));
		AttachListBefore(*nowy);
		return nowy;
	}

	std::shared_ptr<Loose_Element<t> > emplace_last()
	{
		std::shared_ptr<Loose_Element<t>> nowy(new Loose_Element<t>());
		AttachListBefore(*nowy);
		return nowy;
	}
	
	auto FastIterator()
	{
		struct fast_iterator
		{
			Loose_Node<t> * root;
			Loose_Node<t> * nod;

			fast_iterator(Loose_Node<t> &  l)
			{
				root = &l;
				nod = root->next;
			}

			fast_iterator & begin()
			{
				return *this;
			}
			fast_iterator & end()
			{
				return *this;
			}

			inline void operator++()
			{
				nod = nod->next;
			}
			inline t & operator*()
			{
				//return *(nod->get());
				return static_cast<Loose_Element<t>*>(nod)->element;
			}
			inline bool operator !=(const fast_iterator & b)
			{
				return nod != root;
			}
		};
		return fast_iterator(*this);
	}
	auto SaveIterator()
	{
		struct save_iterator:non_replicable
		{
			Loose_Node<t> before_to_iteration;
			Loose_Node<t> after_to_iteration;


			save_iterator(save_iterator & iter)
			{
				iter.before_to_iteration.AttachListAfter(before_to_iteration);
				iter.after_to_iteration.AttachListAfter(after_to_iteration);
				iter.before_to_iteration.detach();
				iter.after_to_iteration.detach();
			}

			save_iterator(Loose_Node<t> & list)
			{
				list.AttachListAfter(before_to_iteration);
				list.AttachListBefore(after_to_iteration);
			}
			save_iterator & begin()
			{
				return *this;
			}
			save_iterator & end()
			{
				return *this;
			}

			inline void operator++()
			{
			}
			inline t & operator*()
			{
				//auto a = before_to_iteration.next;
				//a->detach();
				//before_to_iteration.AddBefore(*a);
				before_to_iteration.step_foreward();
				return static_cast<Loose_Element<t>*>(before_to_iteration.back)->element;
			}
			inline bool operator !=(const save_iterator & b)
			{
				return before_to_iteration.next != &after_to_iteration;
			}
		};
		return save_iterator(*this);
	}

	virtual typename OwnersipOrganizer<t>::pointer_type get()
	{
		return 0;
	}
	virtual typename OwnersipOrganizer<t>::pointer_to_const_type get()const
	{
		return 0;
	}
};
template<class t>
struct Loose_Element :Loose_Node<t>
{
	t element;
	Loose_Element() = default;
	Loose_Element(typename OwnersipOrganizer<t>::copying_type e) :element(e)
	{

	}

	virtual typename OwnersipOrganizer<t>::pointer_type get()
	{
		return &element;
	}
	virtual typename OwnersipOrganizer<t>::pointer_to_const_type get()const
	{
		return &element;
	}


};


template<class t>
using LooseList = Loose_Node<t>;
template<class t>
using LooseListElment = std::shared_ptr<Loose_Element<t>>;

#define L_val(lista)typename decltype(lista)::type
#define for_List(lista) for(Loose_Element<L_val(lista)>* i = static_cast<Loose_Element<L_val(lista)>*>((lista).next); (Loose_Node<L_val(lista)>*)(i) != &(lista) ; i = static_cast<Loose_Element<L_val(lista)>*>(i->next))
#define for_named_List(name,lista) for(Loose_Element<L_val(lista)>* (name) = static_cast<Loose_Element<L_val(lista)>*>((lista).next); (Loose_Node<L_val(lista)>*)(name) != &(lista) ; (name) = static_cast<Loose_Element<L_val(lista)>*>((name)->next))
