#pragma once

#include <type_traits>
#include <vector>
#include <array>
#include "Helpers.h"

using namespace std;

template<class t, size_t dim> struct multidimensional
{
	multidimensional()
	{
		sizes.fill(0);
	}
	void size(array<size_t, dim> siz)
	{
		sizes = siz;
		size_t sum = 1;
		for (auto a : siz)
			sum *= a;
		acces_helper[0] = 1;
		for (size_t i = 1; i < dim; i++)
		{
			acces_helper[i] = siz[i - 1] * acces_helper[i - 1];
		}

		vv.resize(sum);
	}
	array<size_t, dim> size()const
	{
		return sizes;
	}
	t& operator [](const array<size_t, dim> & i)
	{
		size_t in = 0;
		for (size_t ii = 0; ii < dim; ii++)
		{
			in += acces_helper[ii] * i[ii];
		}
		return vv[in];
	}
	t& get_internal(size_t i)
	{
		return vv[i];
	}
	size_t internal_size()
	{
		return vv.size();
	}
	size_t internal_index(const array<size_t, dim> & i)
	{
		size_t in = 0;
		for (size_t ii = 0; ii < dim; ii++)
		{
			in += acces_helper[ii] * i[ii];
		}
		return in;
	}
	array<size_t, dim> external_index(size_t i)
	{
		array<size_t, dim> to_return;
		for (size_t ii = dim; ii >1; ii--)
		{
			to_return[ii - 1] = i / acces_helper[ii - 1];
			i = i%acces_helper[ii - 1];
		}
		to_return[0] = i;
		return to_return;
	}
	vector<t> vv;

private:
	array<size_t, dim > acces_helper;
	array<size_t, dim> sizes;


};

