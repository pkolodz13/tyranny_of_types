#pragma once
#include <functional>

#include "Helpers.h"
#include "types.h"
#include "pointers.h"

using namespace std;

template<class t>
struct Hierarchy_tree
{

	std::list<Hierarchy_tree<t>> childs;
	t object;
	Hierarchy_tree & add_child()
	{
		childs.push_back(Hierarchy_tree<t>());
		return childs.back();
	}
	std::vector<reference_wrapper<t>> all_childs_objects() 
	{
		std::vector<reference_wrapper<t>> result;
		result.push_back(reference_wrapper<t>(object));
		for (auto & l : childs)
		{
			auto l_chil = l.all_childs_objects();
			result.insert(result.end(), l_chil.begin(), l_chil.end());
		}
		return result;
	}
	

	t * operator->() { return &object;}
	t& get()
	{
		return object;
	}

	struct Iterator
	{
		Hierarchy_tree<t> * node = 0;
		using list_iterator = typename std::list<Hierarchy_tree<t>>::iterator;
		list_iterator last_end;
		std::vector<pair< list_iterator, list_iterator>> lists;
		Iterator(Hierarchy_tree<t> & it)
		{
			node = &it;
		}
		Iterator()
		{
		}
		t & operator *()
		{
			return node->get();
		}

		inline bool is_not_ended()
		{
			return node != 0;
		}
		inline void operator++()
		{
			assert(( "iterator is ended", is_not_ended()));

			if (!node->childs.empty())
			{
				if (!lists.empty())
				{
					lists.back().first++;
				}
				lists.push_back({node->childs.begin(),node->childs.end() });
				node = &(*lists.back().first);
			}
			else
			{
				if (!lists.empty())
				{
					lists.back().first++;
				}
				while (!lists.empty()&&lists.back().first == lists.back().second)
					lists.pop_back();
				if (!lists.empty())
				{
					node = &(*lists.back().first);
				}
				if (lists.empty())
				{
					node = 0;
				}

			}
			
		}
		inline bool operator !=(const Iterator & end)
		{
			return is_not_ended();
		}

	};
	auto begin()
	{
		return Iterator(*this);
	}
	auto end()
	{
		return Iterator();
	}
};

template<class suggested>
struct suggested_Hierarchy_tree
{
	using this_type = suggested_Hierarchy_tree<suggested>;
	using decayed_type = decay_t<suggested>;
	struct sugested_node_base
	{
		virtual storaging<shared_ptr<sugested_node_base>> add_child() = 0;
		virtual suggested get() = 0;
	};
	template< class realized>
	struct sugested_node_concrete :sugested_node_base
	{
		using this_type = sugested_node_concrete< realized>;

		sugested_node_concrete(Hierarchy_tree<realized> & node)
			:node(node)
		{
		}
		virtual suggested get() { return node.get(); }
		Hierarchy_tree<realized> & node;
		storaging<shared_ptr<sugested_node_base>>  add_child()
		{
			auto & added = node.add_child();
			return make_shared<this_type>(added);
		}
	};
	template< class realized>
	struct sugested_node_proxy :sugested_node_base
	{
		using this_type = sugested_node_proxy< realized>;

		sugested_node_proxy(suggested_Hierarchy_tree<realized> & node)
			:node(node)
		{
		}
		virtual suggested get() 
		{ 
			return node.get() ; 
		}
		suggested_Hierarchy_tree<realized> node;
		storaging<shared_ptr<sugested_node_base>> add_child()
		{
			auto added = node.add_child();
			return make_shared<this_type>(added);
		}
	};
	suggested object;
	this_type add_child()
	{
		return this_type(node_manipulator->add_child());
	}
	suggested & get()
	{
		return object;
	};
	decayed_type * operator->() { return &get(); }

	template<class t, class = enable_if_t<is_convertible_v<t&, suggested>>>
	suggested_Hierarchy_tree(Hierarchy_tree<t> & to_manipulate)
		:node_manipulator(make_shared<sugested_node_concrete<t>>(to_manipulate))
		, object(to_manipulate.get())
	{

	}
	suggested_Hierarchy_tree(storaging<shared_ptr<sugested_node_base>> & child)
		:node_manipulator(child), object(child->get())
	{

	}
	template<class t, class = enable_if_t<is_convertible_v<t&, suggested>>>
	suggested_Hierarchy_tree(suggested_Hierarchy_tree<t> & manipulated)
		: node_manipulator(make_shared<sugested_node_proxy<t>>(manipulated))
		, object(manipulated.get())
	{

	}	
	suggested_Hierarchy_tree(suggested_Hierarchy_tree<suggested> & manipulated)
		: node_manipulator(manipulated.node_manipulator)
		, object(manipulated.object)
	{

	}

	storaging<shared_ptr<sugested_node_base>> node_manipulator;
};