#pragma once
#include "Algo_Pack.h"



template<class t, size_t number>
struct numbered
{
	numbered() {}
	numbered(t  element_) :element(element_)
	{}
	t element;
	t& get()
	{
		return element;
	}
	const t& get()const
	{
		return element;
	}
	using type = t;
	static constexpr size_t value = number;
};
template<class t>
struct numbered<t, 0>
{
	numbered() {}
	numbered(t  element_) :first(element_)
	{}
	t first;
	t& get()
	{
		return first;
	}
	const t& get()const
	{
		return first;
	}
	using type = t;
	static constexpr size_t value = 0;
};
template<class t>
struct numbered<t, 1>
{
	numbered() {}
	numbered(t  element_) :second(element_)
	{}
	t second;
	t& get()
	{
		return second;
	}
	const t& get()const
	{
		return second;
	}
	using type = t;
	static constexpr size_t value = 1;
};
template<class t>
struct numbered<t, 2>
{
	numbered() {}
	numbered(t  element_) :third(element_)
	{}
	t third;
	t& get()
	{
		return third;
	}
	const t& get()const
	{
		return third;
	}
	using type = t;
	static constexpr size_t value = 2;
};

namespace detail_n
{
	template<class t, size_t i>
	struct type_and_number
	{
		using type = t;
		static constexpr size_t value = i;
	};
	template<class t>
	using to_numbered = numbered<typename t::type, t::value>;
	template<class n>
	struct is_not_void
	{
		static constexpr size_t value = !is_same<typename n::type, void>::value;
	};
	template<class p, class seq>
	struct pre_numeration;
	template<class ... t, size_t ... I>
	struct pre_numeration<Pack<t...>, index_sequence<I...>>
	{
		using all_numeric = Pack<type_and_number<t, I>...>;
		using type = p_detail<to_numbered, p_select<is_not_void, all_numeric>>;
	};

	template<class t>
	using to_integral_size = integral_constant<size_t, t::value>;
}


template<class p>
struct pro_numeration;
#pragma region pro_num_definition
template<class ... pp>
struct pro_numeration<Pack<pp...>> :default_constructor,pp...
{
	using bases_p = Pack<pp...>;
	using types_p = p_detail<type_of, bases_p>;
	using integrals_p = p_detail<detail_n::to_integral_size, bases_p>;



	template<class t>
	enable_if_t<p_has<types_p,t>,t&>  get()
	{
		using base_type = p_element<p_index< types_p, t>, bases_p>;
		return this->base_type::get();
	}
	template<class t>
	enable_if_t<p_has<types_p, t>, const t&>  get()const
	{
		using base_type = p_element<p_index< types_p, t>, bases_p>;
		return this->base_type::get();
	}
	template<size_t i>
	auto & get()
	{
		using base_type = p_element<p_index<integrals_p, integral_constant<size_t, i> >, bases_p>;
		return this->base_type::get();
	}
	template<size_t i>
	const auto & get() const 
	{
		using base_type = p_element<p_index<integrals_p,integral_constant<size_t, i> >, bases_p>;
		return this->base_type::get();
	}
	template<class l>
	void for_each(l && lam)
	{
		auto d = { (lam(((pp&)(*this)).get())    ,0)...,0 };
	}
	template<class l>
	auto expand(l && lam)
	{
		return lam(pp::get()...);
	}
	template<class l>
	auto expand(l && lam)const
	{
		return lam(pp::get()...);
	}

	pro_numeration( type_of<pp> ... args) :pp(args)...
	{}
	using default_constructor::default_constructor;
};
#pragma endregion



template<class ... types_t>
struct Numeration :pro_numeration<typename detail_n::pre_numeration<Pack<types_t...>, make_index_sequence<sizeof...(types_t)>>::type >
{
	using pro = pro_numeration<typename detail_n::pre_numeration<Pack<types_t...>, make_index_sequence<sizeof...(types_t)>>::type >;
	using pro::pro_numeration;

	template<size_t i >
	using element = p_element<i, Pack<types_t...>>;
	template<class t >
	using index = P_index<t, Pack<types_t...>>;

	static constexpr size_t size = sizeof...(types_t);
};

template<class N, class too_add>
struct N_collapse;
template<class ...nn, class  n2>
struct N_collapse<Numeration<nn...>, n2>
{
	using type = Numeration<nn..., n2>;
};
template<class ...nn, class n1, class ... nn2>
struct N_collapse< Numeration<nn...>, Numeration<n1, nn2...>>
{
	using type = typename N_collapse<typename N_collapse <Numeration<nn...>, n1>::type, Numeration<nn2...>>::type;
};
template<class ...nn>
struct N_collapse< Numeration<nn...>, Numeration<>>
{
	using type = Numeration<nn...>;
};

template<class ...nn>
using n_collapse = typename N_collapse<Numeration<>, Numeration<nn...>>::type;

template<class numeric, size_t start>
struct N_cut_first;
template<class n1, class ... nn, size_t start>
struct N_cut_first<Numeration<n1, nn...>, start>
{
	using type_true = typename N_cut_first<Numeration<nn...>, start - 1>::type;
	using type_false = Numeration<n1, nn...>;

	using type = conditional_t<start != 0, type_true, type_false>;
};
template< size_t start>
struct N_cut_first<Numeration<>, start>
{
	using type = Numeration<>;
};

template<class n1, class N>
struct N_add_first;
template<class n1, class ... nn>
struct N_add_first<n1, Numeration<nn...>>
{
	using type = Numeration<n1, nn...>;
};

template<class numeric, size_t end>
struct N_cut_last;
template<class n1, class ... nn, size_t end>
struct N_cut_last<Numeration< n1, nn...>, end>
{
	using type_true = typename N_add_first<n1, typename N_cut_last<Numeration<nn...>, end - 1>::type>::type;
	using type_false = Numeration<>;

	using type = conditional_t<end != 0, type_true, type_false>;
};
template< size_t end>
struct N_cut_last<Numeration<>, end>
{
	using type = Numeration<>;
};

template<class num, size_t start, size_t end>
using n_sub = typename N_cut_first< typename N_cut_last<num, end>::type, start>::type;
template<class N, size_t end>
using n_cut_at = typename N_cut_last<N, end>::type;

template<class ... tt>
auto tie_numeration(tt &... args)
{
	return Numeration<tt & ...>(args...);
}
template<class ... tt>
auto make_numeration(tt ... args)
{
	return Numeration<decay_t<tt>...>(args...);
}

template<class ... t_v,class t>
Numeration<t_v..., t> add_to_numeration(const Numeration<t_v...> & num, const t & to_add)
{
	return num.expand([&](auto &&... elems) {return Numeration<t_v..., t>{elems..., to_add}; });
}
template< class t,class ... t_v>
Numeration< t,t_v...> add_to_numeration( const t & to_add, const Numeration<t_v...> & num)
{
	return num.expand([&](auto &&... elems) {return Numeration< t, t_v...>{to_add,elems...}; });
}
template<size_t i,class ... t_v>
auto remove_from_numeration(const Numeration<t_v...> & num)
{
	using indexes_p = p_index_sequence<sizeof...(t_v)>;
	using cleaned_p = p_delete_at<i, indexes_p>;
	using result_type = p_transform<Numeration, p_delete_at<i, Pack<t_v...>>>;
	return cleaned_p::expand([&](auto ... args) 
	{
		return result_type{ num.get<type_of<decltype(args)>::value>()... };
	});
}
