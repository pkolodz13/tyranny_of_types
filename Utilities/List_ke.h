#pragma once

#include <memory>
#include <functional>
#include <type_traits>
#include <map>
#include <vector>
#include <variant>
#include <cassert>

#include "Helpers.h"
#include "types.h"
#include "Numeration.h"
#include "Loose_List.h"

using namespace std;


#pragma region loops


template<size_t ...I, class ... t, class f>
inline void call_for_tuple(index_sequence<I...>, tuple<t...> & t, const f & func)
{
	(func(get<I>(t))  ,...   );
}
template<class ... tt, class f>
inline void for_tuple(tuple<tt...> & t, f func)
{
	call_for_tuple(make_index_sequence<sizeof...(tt)>{}, t, func);
}
namespace mystd
{
	namespace detail
	{
		template<class F, size_t ...I>
		void inline for_index_sequence(F &&f, index_sequence<I...>i)
		{
			 (   (f( integral_constant<size_t,I>() ))   ,...);
		}
	}

	template<size_t Indexes, class lam >
	void inline for_index_sequence(lam && l)
	{
		detail::for_index_sequence(l, make_index_sequence<Indexes>());
	}
}
using namespace mystd;


#pragma endregion

#pragma region numeration

template<size_t passed, class ... tn, class n>
auto n_Collapse_detail(Numeration<tn...>& target, const n & to_collapse)
{
	target.get<passed>() = to_collapse;
}
template<size_t passed, class ... tn, class ... nn2>
auto n_Collapse_detail(Numeration<tn...> & target, const Numeration<nn2...> & to_collapse)
{
	for_index_sequence<sizeof...(nn2)>([&](auto a)
	{
		constexpr size_t i = decltype(a)::value;
		constexpr size_t progress = n_collapse<n_cut_at<Numeration<nn2...>, i>>::size;

		n_Collapse_detail<passed + progress>(target, to_collapse.get<i>());
	}
	);
}
template<class ... nn>
auto n_Collapse(const nn & ... args)
{
	using target_type = n_collapse<nn...>;
	target_type to_return;
	//Numeration<const nn & ... > helper(args...);

	n_Collapse_detail<0>(to_return, tie_numeration(args...));
	//for_index_sequence<sizeof...(nn)>([&](auto a)
	//{
	//	constexpr size_t i = decltype(a)::value;
	//	constexpr size_t progress = n_cut_at<Numeration<nn...>, i>::size;

	//	n_Collapse_detail< progress>(to_return, helper.get<i>());
	//}
	//);

	return to_return;

}
#pragma endregion



#pragma region advanced




template<class t, size_t instance = 0>
struct Singleton
{
	static t& Instance()
	{
		static t inst;
		return inst;
	}
};

template<template<class>class representative_of, class t_p>
struct Representatives_by_pack:p_transform<Numeration,p_detail<representative_of,t_p>>
{
	using base_type = p_transform<Numeration, p_detail<representative_of, t_p>>;
	using base_type::Numeration;
	using base_type::get;
	using represented_p = t_p;
	using representing_p = p_detail<representative_of, t_p>;

	template<class t>
	representative_of<t> & get()
	{
		static_assert(p_has<t_p, t>, "called type is no represented");
		return this->get<p_index<t_p, t>>();
	}
	template<class t>
	const representative_of<t> & get()const
	{
		static_assert(p_has<t_p, t>, "called type is no represented");
		return this->get<p_index<t_p, t>>();
	}
	template<size_t i>
	representative_of<p_element<i,t_p>> & get()
	{
		return this->base_type::get<i>();
	}
	template<size_t i>
	const representative_of<p_element<i, t_p>> & get()const
	{
		return this->base_type::get<i>();
	}
};
template<template<class>class representative_of, class ... t_v>
using Representatives = Representatives_by_pack<representative_of,Pack<t_v...>>;

template<class ... tt>
struct varaint_dynamic :non_replicable
{
	template<class t,class ... args>
	void emplace(const args & ... args_a)
	{
		object.emplace<unique_ptr<t>>(make_unique<t>(args_a...));
	}
	variant<unique_ptr<tt>... > object;
	template<class t>
	bool holds_alternative()
	{
		return object.holds_alternative<unique_ptr<t>>();
	}
	size_t index()
	{
		return object.index();
	}
	template<class lam>
	auto visit(const lam & func)
	{
		return std::visit([&](auto & a){
			return func(*a);
		}, object);
		//if constexpr (sizeof...(tt) > 0)
		//{
		//	using visitor_type = decltype(&varaint_dynamic<tt...>::template call_visit<p_element<0,Pack<tt...>>,lam>);
		//	array<visitor_type, sizeof...(tt) > visitors = { &varaint_dynamic<tt...>::template call_visit<tt,lam>... };
		//	return (this->*visitors[index()])(func);

		//}
	}
	template<class t>
	t& get()
	{
		return *std::get<unique_ptr<t>>(object);
	}
private:
	template<class t, class lam>
	auto call_visit(const lam & func)
	{
		return func(this->get<t>());
	}

};

#pragma endregion