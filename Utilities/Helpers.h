#pragma once
#include <type_traits>
#include <vector>
#include <list>

#define for_range(size) for(std::decay_t<decltype(size)> i = 0;i< (size) ;i++)
#define for_section(start,end) for(std::decay_t<decltype(start)> i = (start) ;i< (end) ;i++)
#define for_named_range(name,size) for(std::decay_t<decltype(size)> name = 0; name < (size) ; name++)
#define for_named_section(name,start,end) for(std::decay_t<decltype(start)> name = (start); name <(end); name ++)

template<class t>
using vector_t = std::vector<t>;
template<class t>
using list_t = std::list<t>;

struct non_copyable
{
	non_copyable() = default;
	non_copyable(const non_copyable &) = delete;
	non_copyable & operator =(const non_copyable &) = delete;
};
struct non_movable
{
	non_movable() = default;
	non_movable(non_movable &&) = delete;
	non_movable & operator =(non_movable &&) = delete;
};
struct non_replicable:non_copyable, non_movable
{
};
struct default_constructor
{
};
template<class ... bases_v>
struct variadic_inheritance : bases_v...
{};