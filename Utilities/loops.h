#pragma once
#include <fstream>
#include <sstream>
#include <memory>
#include <array>
#include <queue>

#include "Multidimensional.h"

struct size_Iterator
{
	size_t size;
	size_t i;
	size_Iterator(size_t _size)
	{
		i = 0;
		size = _size;
	}
	size_Iterator(size_t start_value, size_t _size)
	{
		i = start_value;
		size = _size + start_value;
	}
	size_Iterator & begin()
	{
		return *this;
	}
	size_Iterator & end()
	{
		return *this;
	}
	inline void operator++()
	{
		i++;
	}
	inline size_t operator*()
	{
		return i;
	}
	inline bool operator !=(const size_Iterator & b)
	{
		return i < size;
	}


};
template<size_t dim>
struct unrepeatable_Iterator
{
	size_t size;
	array<size_t, dim> i;
	unrepeatable_Iterator(size_t _size)
	{
		for (auto a : size_Iterator(dim))
			i[a] = 0;
		i[0] = 1;
		size = _size;
	}
	unrepeatable_Iterator & begin()
	{
		return *this;
	}
	unrepeatable_Iterator & end()
	{
		return *this;
	}
	inline void operator++()
	{
		i[0]++;
		for (auto a : size_Iterator(dim - 1))
		{
			if (i[a] >= size)
			{
				i[a + 1]++;
				i[a] = i[a + 1] + 1;
			}
		}
	}
	inline array<size_t, dim> operator*()
	{
		return i;
	}
	inline bool operator !=(const  unrepeatable_Iterator & b)
	{
		return i[0] < size;
	}


};

template<size_t dim>
struct multidimensional_Iterator
{
	array< size_t, dim> size;
	array< size_t, dim> start;
	array<size_t, dim > acces_helper;
	size_t sum;
	size_t ii;
	multidimensional_Iterator()
	{

	}
	multidimensional_Iterator(array<size_t, dim> siz)
	{
		start.fill(0);
		ii = 0;
		size = siz;
		sum = 1;
		for (auto a : siz)
			sum *= a;
		acces_helper[0] = 1;
		for (size_t i = 1; i < dim; i++)
		{
			acces_helper[i] = siz[i - 1] * acces_helper[i - 1];
		}
	}
	multidimensional_Iterator(array<size_t, dim> _start, array<size_t, dim> siz)
	{
		start = _start;
		ii = 0;
		size = siz;
		sum = 1;
		for (auto a : siz)
			sum *= a;
		acces_helper[0] = 1;
		for (size_t i = 1; i < dim; i++)
		{
			acces_helper[i] = siz[i - 1] * acces_helper[i - 1];
		}
	}
	multidimensional_Iterator & begin()
	{
		ii = 0;
		return *this;
	}
	multidimensional_Iterator & end()
	{
		return *this;
	}
	inline void operator++()
	{
		ii++;
	}
	inline array<size_t, dim> operator*()
	{
		size_t i = ii;
		array<size_t, dim> to_return;
		for (size_t ii = dim; ii > 1; ii--)
		{
			to_return[ii - 1] = i / acces_helper[ii - 1] + start[ii - 1];
			i = i % acces_helper[ii - 1];
		}
		to_return[0] = i + start[0];
		return to_return;
	}
	inline bool operator !=(const  multidimensional_Iterator & b)
	{
		return ii < sum;
	}


};

template<size_t dim>
auto mult_iterator(array<size_t, dim> siz)
{
	return multidimensional_Iterator<dim>(siz);
}

template<size_t dim>
struct cecular_automator_cutator
{
	array<size_t, dim> size;
	struct region_data
	{
		vector<array<int, dim>> neighbors;
		array<size_t, dim> start;
		array<size_t, dim> size;
		auto  begin()
		{
			return multidimensional_Iterator<dim>(start, size);
		}
		auto  end()
		{
			return multidimensional_Iterator<dim>(start, size);
		}
	};
	vector<region_data> regions;
	cecular_automator_cutator(array<size_t, dim> size_, vector<array<int, dim>> neightbours)
	{
		for (size_t i = 0; i < neightbours.size(); i++)
		{
			for (auto a : size_Iterator(dim))
			{
				if ((neightbours[i][a] >= (int)size_[a]) || (-neightbours[i][a] >= (int)size_[a]))
				{
					neightbours.erase(neightbours.begin() + i);
					i--;
					break;
				}
			}
		}
		array<vector<int>, dim   > stages_;
		for (auto & a : stages_)
		{
			a.push_back(0);
		}
		for (auto & a : neightbours)
		{
			for (auto b : size_Iterator(dim))
			{
				bool contain = false;
				for (auto c : stages_[b])
					if (c == a[b])
					{
						contain = true;
						break;
					}
				if (!contain)
				{
					stages_[b].push_back(a[b]);
				}
			}
		}
		size_t regions_size = 1;
		array<size_t, dim   > ms;
		for (auto a : size_Iterator(dim))
		{
			std::sort(stages_[a].begin(), stages_[a].end());
			regions_size *= stages_[a].size();
			ms[a] = stages_[a].size();
		}
		vector<array<int, dim>> stages2;
		stages2.reserve(regions_size);
		for (auto a : multidimensional_Iterator<dim>(ms))
		{
			array<int, dim> to_add;
			for (auto b : size_Iterator(dim))
				to_add[b] = stages_[b][a[b]];

			stages2.push_back(to_add);
		}
		regions.resize(regions_size);
		vector<bool> neig_flags(neightbours.size());
		for (auto a : size_Iterator(regions_size))
		{
			fill(neig_flags.begin(), neig_flags.end(), true);
			auto & region = regions[a];
			region.neighbors.reserve(neightbours.size());

			for (auto b : size_Iterator(dim))
			{
				auto pom = stages2[a][b];
				if (pom < 0)
				{
					for (auto c : size_Iterator(neightbours.size()))
					{
						if (neightbours[c][b] <= pom)
							neig_flags[c] = false;

					}
					for (auto c : size_Iterator(stages_[b].size()))
					{
						if (stages_[b][c] == pom)
						{
							region.size[b] = stages_[b][c + 1] - pom;
						}
					}
					region.start[b] = pom - stages_[b].front();
				}
				else if (pom == 0)
				{
					region.start[b] = -stages_[b].front();
					region.size[b] = size_[b] - stages_[b].back() - region.start[b];
				}
				else//stages2[a][b]>0
				{
					for (auto c : size_Iterator(neightbours.size()))
					{
						if (neightbours[c][b] >= pom)
							neig_flags[c] = false;
					}
					for (auto c : size_Iterator(stages_[b].size()))
					{
						if (stages_[b][c] == pom)
						{
							region.size[b] = pom - stages_[b][c - 1];
							region.start[b] = size_[b] - (pom - stages_[b][c - 1]);
						}
					}
				}

			}
			for (auto b : size_Iterator(neightbours.size()))
			{
				if (neig_flags[b])
					region.neighbors.push_back(neightbours[b]);
			}

		}

	}
	auto  begin()
	{
		return regions.begin();
	}
	auto  end()
	{
		return regions.end();
		//return *this;
	}
	inline void operator++()
	{
	}
	inline region_data & operator*()
	{
	}
	inline bool operator !=(const  cecular_automator_cutator & b)
	{
	}

};



template<class Mesh>
float surface(const Mesh & mesh)
{
	float sum = 0;
	for (auto i : size_Iterator(mesh.Size()))
	{
		auto pom = mesh.get_primitive(i);
		//sum += triangleField(pom[0].get<vertexA>(),pom[1].get<vertexA>(),pom[2].get<vertexA>());
	}
	return sum;
}

template<size_t dim>
vector<array<int, dim>> surroundings()
{
	array<int, 3> a = { -1,0,1 };
	array<size_t, dim> tab;
	tab.fill(3);
	vector<array<int, dim>> to_return;
	for (auto b : multidimensional_Iterator<dim>(tab))
	{
		array<int, dim> to_add;
		bool zero = false;
		for (auto c : size_Iterator(dim))
		{
			if (a[b[c]] != 0)
				zero = true;
			to_add[c] = a[b[c]];
		}
		if (zero)
			to_return.push_back(to_add);
	}

	return to_return;
}
template<size_t dim>
vector<array<int, dim>> enviroment()
{
	array<int, 3> a = { -1,0,1 };
	array<size_t, dim> tab;
	tab.fill(3);
	vector<array<int, dim>> to_return;
	for (auto b : multidimensional_Iterator<dim>(tab))
	{
		array<int, dim> to_add;
		for (auto c : size_Iterator(dim))
		{
			to_add[c] = a[b[c]];
		}
		to_return.push_back(to_add);
	}

	return to_return;
}


template<class t1, class t2, size_t dim>
array<t1, dim> operator +(const array<t1, dim> & t1_, const array<t2, dim> & t2_)
{
	array<t1, dim> to_return;
	for (size_t i = 0; i < dim; i++)
	{
		to_return[i] = t1_[i] + t2_[i];
	}
	return to_return;
}
