#pragma once
#include <type_traits>
#include <functional>
#include <tuple>
#include "types.h"
using namespace std;

namespace typ
{
	template< class ...tt> 
	struct Pack 
	{
		static constexpr size_t size = sizeof...(tt);
		template<class lam>
		static void for_each(lam && f)
		{
			( f(Overtype<tt>()) ,...);
		}
		template<class lam>
		static auto expand(lam && f)
		{
			return f(Overtype<tt>()...);
		}
	};

#pragma region basic

	template <typename T, typename p> 
	struct P_has;
	template <typename T> 
	struct P_has<T, Pack<>> : std::false_type {};
	template <typename T, typename U, typename... Ts> 
	struct P_has<T, Pack<U, Ts...>> : P_has<T, Pack<Ts...>> {};
	template <typename T, typename... Ts> 
	struct P_has<T, Pack<T, Ts...>> : std::true_type {};

	template<class p, class t>
	constexpr bool p_has = P_has<t,p>::value;


	template <class T, class P>
	struct P_index;
	template <class T, class... Ts>
	struct P_index<T, Pack< T, Ts...>> : std::integral_constant<std::size_t, 0> {};
	template <class T, class U, class... Ts>
	struct P_index<T, Pack<U, Ts...>> : std::integral_constant<std::size_t, 1 + P_index<T, Pack< Ts...>>::value> 
	{static_assert(p_has<Pack<U, Ts...>, T>,"pack hasn't asked type");};
	template <class T, class ... potential_empty>
	struct P_index<T, Pack<potential_empty...>> { static_assert(std::is_same_v<Pack<potential_empty...>,Pack<>>, "pack is empty"); };

	template<class p, class t>
	constexpr size_t p_index = P_index<t,p>::value;

	template<class p> struct P_size;
	template<class ...s> struct P_size<Pack<s...>> :integral_constant<size_t, sizeof...(s)>
	{};

	template<size_t n, class p> 
	struct P_element;
	template<size_t n, class e1, class ... e> 
	struct P_element<n, Pack<e1, e...>>
	{
		using type = typename conditional<n == 0, e1, typename P_element<n - 1, Pack< e...>>::type>::type;
	};
	template<size_t n, class e1 > 
	struct P_element<n, Pack<e1>>
	{
		using type = e1;
	};

	template <size_t n, class p>
	using p_element = typename P_element<n, p>::type;


	template<class t> 
	struct P_is: std::false_type{};
	template<class ... pp> 
	struct P_is<Pack<pp...>> : std::true_type{};


	template<class p>
	struct P_ensure
	{
		using type = Pack<p>;
	};
	template<class ... pp>
	struct P_ensure<Pack<pp...>>
	{
		using type = Pack<pp ...>;
	};
	template<class p>
	using p_ensure = typename P_ensure<p>::type;



	template<class p1,class p2>
	struct P_matches;
	template<class ...v1,class ... v2>
	struct P_matches<Pack<v1...>, Pack<v2...>>
	{
		static constexpr bool result()
		{
			return ((p_has<Pack<v1...>, v2>) && ...) && ((p_has<Pack<v2...>, v1>) && ...);
		}
		static constexpr bool value = result();
	};


	template<class p1, class p2>
	constexpr bool p_matches = P_matches<p1, p2>::value;
#pragma endregion

#pragma region arithmetic logic
	template< class t1, class t2> 
	struct P_add
	{
		using type = Pack <t1, t2 >;
	};
	template< class ... tt1, class ... tt2>
	struct P_add< Pack<tt1...>, Pack<tt2...>>
	{
		using type = Pack < tt1..., tt2... >;
	};
	template< class ... tt, class t2> 
	struct P_add< Pack<tt...>, t2>
	{
		using type = Pack < tt..., t2 >;
	};
	template< class t2, class ... tt> 
	struct P_add< t2, Pack<tt...>>
	{
		using type = Pack <t2, tt... >;
	};
	template<class p,class t>
	using p_add = typename P_add<p, t>::type;


	template<class ...tt>
	struct P_fusion;
	template<class t1, class ... tt>
	struct P_fusion< t1,tt...>
	{
		using type = p_add< t1,typename  P_fusion<tt...>::type>;
	};
	template<>
	struct P_fusion<>
	{
		using type = Pack<>;
	};
	template<class ... pp>
	using p_fusion = typename P_fusion<pp...>::type;


	template<class p> struct P_exclusion;
	template<class p1, class ...pp> struct P_exclusion<Pack<p1,pp...>>
	{
		using exluted =typename  P_exclusion<Pack<pp...>>::type;
		using type = conditional_t<P_has<p1, exluted>::value, exluted, p_add<exluted, p1>>;
	};
	template<> struct P_exclusion<Pack<>>
	{
		using type = Pack<>;
	};
	template<class p>
	using p_exclusion = typename P_exclusion<p>::type;


	template<size_t i, class p>struct P_delete_at_position;
	template<size_t i, class p, class ...pp>struct P_delete_at_position<i, Pack<p, pp...>>
	{
		using type = conditional_t< i == 0, Pack<pp...>, p_fusion < Pack<p>, typename P_delete_at_position<i - 1, Pack<pp...>>::type>>;
	};
	template<size_t i>struct P_delete_at_position<i, Pack<>>
	{
		using type = Pack<>;
	};
	template<size_t i,class p>
	using p_delete_at = typename P_delete_at_position<i, p>::type;

	template< class t, class p, class Enable = void>struct P_delete_type;
	template< class t, class p>struct P_delete_type< t, p, enable_if_t<p_has<p,t  >>>
	{
		using del_type = typename  P_delete_at_position< P_index<t, p>::value, p >::type;
		using type = typename P_delete_type<t, p>::type;
	};
	template< class t, class p>struct P_delete_type< t, p, enable_if_t<!p_has<p, t  >>>
	{
		using type = p;
	};
	template <class t, class p>
	using p_delete_type = typename P_delete_type<t, p>::type;

	template <class p1, class p2>struct P_conjunction;
	template <class p1, class pc, class ... pp>struct P_conjunction<p1, Pack<pc, pp...>>
	{
		using type = conditional_t<P_has<pc, p1>::value, p_fusion<Pack<pc>, typename P_conjunction<p1, Pack<pp...>>::type>, typename P_conjunction<p1, Pack<pp...>>::type>;
	};
	template <class p1>struct P_conjunction<p1, Pack<>>
	{
		using type = Pack<>;
	};

	template<class p1, class p2>
	using p_conjunction = p_exclusion<  typename  P_conjunction<p1, p2>::type>;

	template< class det, class main_pack>struct P_detach;
	template<class det, class m1, class ...mm>struct P_detach< det, Pack<m1, mm...>  >
	{
		using pom = typename P_detach<det, Pack<mm...>>::type;
		using type = conditional_t<P_has<m1, det>::value, pom, p_fusion<Pack<m1>, pom>>;
	};
	template<class det>struct P_detach< det, Pack<>  >
	{
		using type = Pack<>;
	};


	template<class main_pack, class det>
	using p_detach = typename P_detach< det, main_pack >::type;

	template<class numeric, size_t start>
	struct P_cut_first;
	template<class n1, class ... nn, size_t start>
	struct P_cut_first<Pack<n1, nn...>, start>
	{
		using type = typename P_cut_first<Pack<nn...>, start - 1>::type;
	};
	template<class n1, class ... nn>
	struct P_cut_first<Pack<n1, nn...>, 0>
	{
		using type = Pack<n1, nn...>;
	};
	template< >
	struct P_cut_first<Pack<>, 0>
	{
		using type = Pack<>;
	};

	template<class numeric, size_t end>
	struct P_cut_last;
	template<class n1, class ... nn, size_t end>
	struct P_cut_last<Pack< n1, nn...>, end>
	{
		using type = p_add<n1, typename P_cut_last<Pack<nn...>, end - 1>::type>;

	};
	template<class n1, class ... nn>
	struct P_cut_last<Pack<n1, nn...>, 0>
	{
		using type = Pack<>;
	};
	template< >
	struct P_cut_last<Pack<  >, 0>
	{
		using type = Pack<>;
	};

	template<class num, size_t start, size_t end>
	using p_sub = typename P_cut_first< typename P_cut_last<num, end>::type, start>::type;

	template<class p1,class p2>
	using p_disjunction = p_exclusion<p_fusion<p1, p2>>;
#pragma endregion

#pragma region transforming


	template<class n>
	struct P_from;
	template<template<class ...>class temp, class ... tt>
	struct P_from<temp<tt...>>
	{
		using type = Pack<tt...>;
	};

	template<class t>
	using p_from = typename P_from<t>::type;

	template<template<typename > class  _t, class t> struct P_detail;
	template<template<typename> class  _t, class e, class ... ee > struct P_detail< _t, Pack<e, ee ...>  >
	{
		using type = typename P_fusion< Pack<_t<e>>, typename P_detail<_t, Pack<ee...>>::type>::type;
	};
	template<template<typename> class  _t > struct P_detail< _t, Pack<>  >
	{
		using type = Pack<>;
	};
	template<template<typename> class  _t, class t>
	using p_detail = typename P_detail<_t, t>::type;

	template<template<typename> class  c, class t>struct P_select;
	template<template<typename> class  c, class p1, class ...pp>struct P_select<c, Pack<p1, pp...>>
	{
		using pom = typename P_select<c, Pack<pp...>>::type;
		using type = conditional_t<c<p1>::value
			, p_fusion<Pack<p1>, pom>
			, pom>;
	};
	template<template<typename> class  c> struct P_select<c, Pack<>>
	{
		using type = Pack<>;
	};

	template<template<typename> class  c, class p>
	using p_select = typename P_select<c, p>::type;


	template<template<typename ... >class t, class p>struct P_transfrom;
	template<template<typename ... >class t, class ...pp>struct P_transfrom<t, Pack<pp...>>
	{
		using type = t<pp...>;
	};
	template<template<typename ... >class t, class p>
	using p_transform = typename P_transfrom<t, p>::type;

	template<class p>
	using p_tuple = p_transform<tuple,p>;

	template<class t> struct T_pack;
	template< class ... tt> struct  T_pack<tuple< tt...>>
	{
		using type = Pack<tt...>;
	};
	template<class _p>
	using t_pack = typename T_pack<_p>::type;


	template<class p>
	struct P_collapse
	{
		using type = Pack<p>;
	};
	template<class ... pp>
	struct P_collapse<Pack<pp...>>
	{
		using type = p_fusion<typename  P_collapse<pp>::type ...>;
	};

	template<class ... pp>
	using p_collapse = typename P_collapse<Pack<pp...> >::type;
	template<class t , class sequence>
	struct P_array;
	template<class t,size_t ... I >
	struct P_array<t, std::index_sequence<I...>>
	{
		template<size_t i>
		using i_type = t;
		using type = Pack<i_type<I>...>;
	};
	template<class t,size_t size>
	using p_array = typename P_array<t, make_index_sequence<size>>::type;
#pragma endregion


#pragma region inheriting logic

	template<class s, class p> struct BaseTypes;
	template<class s, class b1, class ...bb> struct BaseTypes<s, Pack<b1, bb...>>
	{
		using type = conditional_t<is_base_of_v<b1, s>, p_fusion<Pack<b1>, typename BaseTypes<s, Pack<bb...>>::type>, typename BaseTypes<s, Pack<bb...>>::type>;
	};
	template<class s> struct BaseTypes<s, Pack<>>
	{
		using type = Pack<>;
	};

	template<class s, class p> struct InheritingTypes;
	template<class s, class b1, class ...bb> struct InheritingTypes<s, Pack<b1, bb...>>
	{
		using type = conditional_t<is_base_of_v< s, b1>, p_fusion<Pack<b1>, typename InheritingTypes<s, Pack<bb...>>::type>, typename InheritingTypes<s, Pack<bb...>>::type>;
	};
	template<class s> struct InheritingTypes<s, Pack<>>
	{
		using type = Pack<>;
	};

	template< class t, class ... tt> struct BaseType;
	template< class t, class t1, class ... tt> struct BaseType< t, t1, tt...>
	{
		using type = typename conditional< is_same_v<t, typename InheritingTypes<t1, t>::type>, t1, typename BaseType<t, tt...>::type >::type;
	};
	template< class t1, class ... tt> struct BaseType< Pack<t1, tt...>>
	{
		using type = t1;
	};

	template<class t> struct BaseTypeOfPack;
	template<class ... tt> struct BaseTypeOfPack<Pack<tt...>>
	{
		using type = typename BaseType<Pack<tt...>, tt...>::type;
	};



	template<class p>
	using baseType = typename BaseTypeOfPack<p>::type;

	template<class s, class p>
	using base_types = typename BaseTypes<s, p>::type;

	template<class s, class p>
	using inheriting_types = typename InheritingTypes<s, p>::type;

#pragma endregion

	template<class t>
	struct P_index_sequence;
	template<size_t ... i_v>
	struct P_index_sequence<std::index_sequence<i_v...>>
	{
		using type = Pack<integral_constant<size_t, i_v>...>;
	};

	template<size_t I>
	using p_index_sequence = typename P_index_sequence<make_index_sequence<I>>::type;
}

#define packalize_member(member)    \
	has_member_type(member);																							\
template<typename T, class Enable = void  >																				\
struct Packalize_##member{ using type = Pack<>; };																		\
template<typename T>																									\
struct Packalize_##member<T, enable_if_t<has_##member<T>::value>>														\
{																														\
	using type = conditional_t<P_is<typename T::##member>::value, typename T::##member, Pack<typename T::##member>   >; \
};																														\
template<class t> using packalize_##member = typename Packalize_##member<t>::type;


using namespace typ;

template<class func>
struct function_prametrization;

template<class result_t, class ... arguments_t_v>
struct function_prametrization<result_t(arguments_t_v...)>
{
	using result_type = result_t;
	using arguments_types = Pack<arguments_t_v...>;
};
template<class result_t, class ... arguments_t_v>
struct function_prametrization< std::function<result_t(arguments_t_v...)>>
{
	using result_type = result_t;
	using arguments_types = Pack<arguments_t_v...>;
};

