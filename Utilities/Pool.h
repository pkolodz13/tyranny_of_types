#pragma once

#include <type_traits>
#include <vector>
#include <array>
#include <functional>
#include "Helpers.h"
#include <set>

using namespace std;

struct indexes_pool
{
	set<int> holes;
	size_t pool_size = 0;
	size_t add()
	{
		if (!holes.empty())
		{
			size_t index = *holes.begin();
			holes.erase(index);
			return index;
		}
		else
		{
			return pool_size++;
		}
	}
	void free(size_t i) 
	{
		holes.insert(i);
	}
	size_t size()
	{
		return pool_size;
	}
	size_t free_size() 
	{
		return holes.size();
	}
	void add_free(size_t siz)
	{
		for_range(siz)
		{
			holes.insert(pool_size++);
		}
	}
	void fit_to_last()
	{
		while (holes.count(pool_size -1))
		{
			holes.erase(--pool_size);
		}
	}
	void clear()
	{
		pool_size = 0;
		holes.clear();
	}
	bool is_free_at(size_t i)
	{
		if (i < pool_size&&holes.count(i) == 0)
		{
			return false;
		}
		return true;
	}
	bool try_add_at(size_t i)
	{
		if (i < pool_size)
		{
			if (holes.count(i) == 1)
			{
				holes.erase(i);
				return true;
			}
			return false;
		}
		size_t p_s = size();
		add_free(i- p_s);
		pool_size++;
		return true;
	}
};

template <class t>
struct Pool:indexes_pool
{
	vector<t> elements;
	indexes_pool indexes;

	Pool() = default;
	Pool(size_t initial_size)
	{
		elements.resize(initial_size);
		indexes.add_free(initial_size);
	}




	size_t add(const t& element)
	{
		size_t added = indexes.add();
		if (elements.size() <= added)
			elements.resize(added + 1);
		return added;

	}
	void free(size_t index)
	{
		indexes.free(index);
	}

	t& operator[](size_t i)
	{
		return elements[i];
	}
	const t& operator[](size_t i)const
	{
		return elements[i];
	}
	size_t size()
	{
		return indexes.size();
	}
	size_t free_size()
	{
		return indexes.free_size();
	}

	size_t active_size()const
	{
		return size() - free_size();
	}

	void clear()
	{
		elements.clear();
		indexes.clear();
	}

	bool try_add_at(size_t index, const t & eventual)
	{
		bool success =  indexes.try_add_at(index);
		if (success)
		{
			if (elements.size() <= index)
				elements.resize(index + 1);
			elements[index] = eventual;
		}

		return success;
	}
};


