#pragma once
#include <type_traits>

template<class t>
using type_of = typename t::type;

template<class base, class derivered>
using is_base_or_same = std::integral_constant<bool, std::is_base_of_v<base, derivered> || std::is_same_v<base, derivered>>;
template<class base, class derivered>
constexpr bool is_base_or_same_v = is_base_or_same<base, derivered>::value;


template<class type, class member_type>
using member_pointer = member_type type::*;
template<class t>
struct OwnersipOrganizer
{
	using pointer_type = t * ;
	using pointer_to_const_type = const t*;
	using copying_type = const t&;
	using reference_type = t & ;
};
template<class t>
struct OwnersipOrganizer<t&>
{
	using pointer_type = t * ;
	using pointer_to_const_type = const t*;
	using copying_type = t & ;
	using reference_type = t & ;
};

template<class t>
struct Overtype 
{
	using type = t;
};
template< class t>
struct removeMemberPointer;
template < class t, class base>
struct removeMemberPointer<base t::*>
{
	using type = base;
};
template <class t>
using remove_m_ptr = typename removeMemberPointer<t>::type;

template <class t>
using removeMember = typename  removeMemberPointer<t>::type;

template<class t>
using is_non_const_lvalue_refeerence = std::integral_constant<bool, std::is_lvalue_reference_v<t> && (!std::is_const_v<std::remove_reference_t<t>>)>;


template<template<class ... > class T>
struct Single_parameter
{
	template<class t>
	using type = T<t>;
};


template< class t, class = void >
struct is_type_existing :std::false_type {};
template<class t>
struct  is_type_existing<t, decltype(typeid(t), void())> :std::true_type
{
};
template<class t>
using type_existance_t = std::enable_if_t< is_type_existing<t>::value>;


template <template <typename...> class C, typename...Ts>
std::true_type is_template_base_of_impl(const C<Ts...>*);

template <template <typename...> class C>
std::false_type is_template_base_of_impl(...);

template < template <typename...> class C,typename T>
using is_template_base_of = decltype(is_template_base_of_impl<C>(std::declval<T*>()));



namespace std
{//remove when appear in standard
	namespace experimental
	{
		struct nonesuch {
			nonesuch() = delete;
			~nonesuch() = delete;
			nonesuch(nonesuch const&) = delete;
			void operator=(nonesuch const&) = delete;
		};
		namespace detail {
			template <class Default, class AlwaysVoid, template<class...> class Op, class... Args>
			struct detector {
				using value_t = std::false_type;
				using type = Default;
			};

			template <class Default, template<class...> class Op, class... Args>
			struct detector<Default, std::void_t<Op<Args...>>, Op, Args...> {
				// Note that std::void_t is a C++17 feature
				using value_t = std::true_type;
				using type = Op<Args...>;
			};

		} // namespace detail

		template <template<class...> class Op, class... Args>
		using is_detected = typename detail::detector<nonesuch, void, Op, Args...>::value_t;

		template <template<class...> class Op, class... Args>
		using detected_t = typename detail::detector<nonesuch, void, Op, Args...>::type;

		template <class Default, template<class...> class Op, class... Args>
		using detected_or = detail::detector<Default, void, Op, Args...>;
	}
}

#define has_member_function(member)    \
	template<typename T, typename = void> struct Has_##member : std::false_type { }; \
	template<typename T>                  struct Has_##member<T, decltype(T::##member, void())> : std::true_type {  }; \
	template<class t> constexpr bool has_##member##_v  = Has_##member<t>::value;

#define has_member_type(member)    \
	template<class t> using member##_of = typename t::##member;\
	template<class t>  struct has_##member :std::experimental::is_detected<member##_of, t> {};

#define has_member_template(member)    \
	template<class t,class t2> using member##_of = typename t::template member##<t2>;\
	template<class t>  struct has_##member :std::experimental::is_detected<member##_of, t,int> {};

#define s_c_expr static constexpr
#define decl_parameter(name) type_of<decay_t<decltype(name)>>

