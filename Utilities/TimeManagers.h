#pragma once
#include <chrono>
#include <algorithm>

#include "pointers.h"
#include "Invoking.h"


namespace TC//time controler
{

	struct TimeCycle_Transgression :Invoker<>
	{
		uint64_t last_start_time;
		uint64_t cycle_duration;
		void tick(uint64_t real_time)
		{
			if (real_time - last_start_time > cycle_duration)
			{
				last_start_time += cycle_duration;
				invoke();
			}
		}
	};
	struct TimeDevice
	{
		virtual uint64_t time() = 0;
		virtual void tick() = 0;
		Invoker<uint64_t> on_tick;
		LooseListElment<function<void(uint64_t)>> OnTick(function<void(uint64_t)> f)
		{
			return on_tick.Add(f);
		}
		function<void()> Caller() {
			return std::bind(&TimeDevice::tick, this);
		}
	};

	struct RealTime
	{
		std::chrono::system_clock::time_point stex_coordArt;
		RealTime()
		{
			stex_coordArt = std::chrono::system_clock::now();
		}
		uint64_t real_time()
		{
			return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - stex_coordArt).count();
		}
	};
	struct TickTime :RealTime, TimeDevice
	{
		uint64_t last_tick_time = 0;
		virtual void tick()
		{
			last_tick_time = real_time();
			on_tick.invoke(last_tick_time);
		}
		virtual uint64_t time()
		{
			return last_tick_time;
		}
	};
	struct FrameTime : TimeDevice
	{
		uint64_t max_frame_duration = -1;
		uint64_t last_frame_time = 0;
		TickTime ticker;

		virtual void tick()
		{
			uint64_t last_tick = ticker.time();
			ticker.tick();
			last_frame_time += std::min<uint64_t>(ticker.time() - last_tick, max_frame_duration);
			on_tick.invoke(last_frame_time);
		}
		uint64_t time()
		{
			return last_frame_time;
		}


		uint64_t tick_time()
		{
			return ticker.time();
		}
		uint64_t real_time()
		{
			return ticker.real_time();
		}
		auto OnRealTick(function<void(uint64_t)> f)
		{
			return ticker.OnTick(f);
		}
		TimeDevice & realTimeDevice()
		{
			return ticker;
		}

	};


}


struct Progrresser
{
	virtual void proggres(float time) = 0;
};
struct TimerSnipper /*:Endable_Timer*/
{
	TimerSnipper() = default;
	TimerSnipper(uint64_t duration_, holder_ptr<Progrresser> progresser_ = holder_ptr<Progrresser>())
	{
		duration = duration_;
		progresser = progresser_;
	}
	uint64_t duration;
	virtual void update(uint64_t time)
	{
		if (progresser)
			progresser->proggres(float(double(std::min<uint64_t>(time, duration)) / double(duration)));
	}
	//virtual bool is_ended(uint64 time)
	//{
	//	return time > duration;
	//}
	holder_ptr<Progrresser> progresser;
};
template<class t>
struct Interpolater : Progrresser
{
	Interpolater(const t & stex_coordArt_vertexAlue, const t & end_vertexAlue, t& interpolated_vertexAlue) :vertexAl_interpolated(interpolated_vertexAlue), vertexAl_end(end_vertexAlue), vertexAl_stex_coordArt(stex_coordArt_vertexAlue)
	{

	}
	t vertexAl_stex_coordArt;
	t vertexAl_end;
	t & vertexAl_interpolated;

	//std::function<time_type()> time_source;
	virtual void proggres(float time) {

		vertexAl_interpolated = vertexAl_stex_coordArt * (1.f - time) + vertexAl_end * time;
	}
};

struct TimersSequence
{
	TimersSequence(TC::TimeDevice & timeDevice, vector<holder_ptr<TimerSnipper>> snippers_source, function<void()> on_end_)
	{
		snippers = snippers_source;
		on_end = on_end_;

		stex_coordArt_time = timeDevice.time();
		tickingKeeper = timeDevice.OnTick(std::bind(&TimersSequence::update, this, placeholders::_1));
	}
	LooseListElment<function<void(uint64_t)>> tickingKeeper;
	uint64_t stex_coordArt_time;
	size_t i = 0;
	virtual void update(uint64_t time)
	{
		while (i<snippers.size() && (time - stex_coordArt_time) >= snippers[i]->duration)
		{
			snippers[i]->update(snippers[i]->duration);
			stex_coordArt_time += snippers[i]->duration;
			i++;
		}
		if (i < snippers.size())
		{
			snippers[i]->update(time - stex_coordArt_time);
		}
		else
		{
			tickingKeeper.reset();
			if (on_end)
				on_end();
		}
	}
	function<void()> on_end;
	vector<holder_ptr<TimerSnipper>> snippers;
};