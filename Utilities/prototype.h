#pragma once
#include <memory>
#include "Helpers.h"

template<class t>
struct prototype
{
private:
	struct abstract_accessor_copier
	{
		virtual std::shared_ptr<abstract_accessor_copier> copy()const = 0;
		virtual const t & get()const = 0;
		virtual t & get() = 0;
	};
	template<class inherited>
	struct accessor_copier:abstract_accessor_copier
	{
		accessor_copier(const inherited & to_copy):object(to_copy) {}
		inherited object;
		virtual std::shared_ptr<abstract_accessor_copier> copy()const
		{
			return std::make_shared<accessor_copier<inherited>>(object);
		}
		virtual const t & get()const
		{
			return object;
		}
		virtual t & get()
		{
			return object;
		}
	};

	std::shared_ptr<abstract_accessor_copier> proto_object;
public:
	prototype() :proto_object(std::make_shared<accessor_copier<t>>(t{})) {}
	template<class inherited,class Enable = std::enable_if_t<std::is_base_of_v<t, inherited>>>
	prototype(const inherited & to_copy) : proto_object(std::make_shared<accessor_copier<inherited>>(to_copy)) {}
	prototype(const t & to_copy) :proto_object(std::make_shared<accessor_copier<t>>(to_copy)) {}
	prototype(const prototype<t> & to_copy):proto_object(to_copy.proto_object->copy()){}
	prototype & operator=(const prototype<t> & to_copy) 
	{ 
		proto_object = to_copy.proto_object->copy(); 
		return *this;
	}


	template<class inherited, class Enable = std::enable_if_t<std::is_base_of_v<t, inherited>>>
	void emplace(const inherited & to_copy) { proto_object = std::make_shared<accessor_copier<inherited>>(to_copy); }
	t * operator->() 
	{
		return &proto_object->get();
	}
	t& get() 
	{
		return proto_object->get();
	}
};