#pragma once
#include <array>
#include <any>
#include <functional>

#include <Utilities/types.h>
#include <Utilities/Algo_Pack.h>
#include <Utilities/Debug.h>


template<class t>
struct holder_ptr
{
	using this_type = holder_ptr<t>;
	holder_ptr()
	{
		direct = 0;
	}
	template<class t2,class = enable_if_t<is_base_or_same_v<t,t2>>>
	holder_ptr(t2 * ptr)
	{
		direct = ptr;
	}
	template<class t2, class = enable_if_t<is_base_or_same_v<t, t2>>>
	holder_ptr(shared_ptr<t2> ptr)
		:owner(ptr), direct(ptr.get())
	{
	}
	template<class t2, class = enable_if_t<is_base_or_same_v<t, t2>>>
	holder_ptr(holder_ptr<t2> ptr)
		: owner(ptr.owner), direct(ptr.direct)
	{
	}
	template<class t2, class = enable_if_t<is_base_or_same_v<t, t2>>>
	this_type & operator =(t2 * ptr)
	{
		owner.reset();
		direct = ptr;
		return *this;
	}
	template<class t2, class = enable_if_t<is_base_or_same_v<t, t2>>>
	this_type & operator = (shared_ptr<t2> ptr)
	{
		owner=ptr;
		direct = ptr.get();
		return *this;
	}
	template<class t2, class = enable_if_t<is_base_or_same_v<t, t2>>>
	this_type & operator = (holder_ptr<t2> ptr)
	{
		owner = ptr.owner;
		direct = ptr.direct;
		return *this;
	}



	operator bool()const
	{
		return direct;
	}
	bool operator !()const
	{
		return direct==0;
	}
	t * operator ->()
	{
		assert(("not storaging anything",direct != 0 ));
		return direct;
	}
	t & operator *()
	{
		assert(("not storaging anything",direct != 0 ));
		return *direct;
	}

	inline t * get()const
	{
		return direct;
	}
	void reset()
	{
		owner.reset();
		direct.reset();
	}
private:
	shared_ptr<t> owner;
	t* direct;
};

template<class pointer_t>
struct storaging;

template<class t>
struct storaging<holder_ptr<t>>
{
	using ptr_type = holder_ptr<t>;
	using this_type = storaging<holder_ptr<t>>;
	template<class stored_type, class ... args_v,class = enable_if_t<is_constructible_v<stored_type, args_v&&...>>>
	storaging(Overtype<stored_type>, args_v && ... args_av)
	{
		ptr = make_shared<stored_type>(std::forward<args_v>(args_av) ...);
	}
	template< class ... args_v, class = enable_if_t<is_constructible_v<t, args_v&&...>>>
	storaging( args_v && ... args_av)
	{
		ptr = make_shared<t>(std::forward<args_v>(args_av) ...);
	}


	template<class t2,class = enable_if_t<is_convertible_v<t2, ptr_type>>>
	storaging(t2 ptr_a)
		:ptr(ptr_a)
	{
		if (!ptr)
			throw "pointer cannot be empty";
	}
	template<class t2, class = enable_if_t<is_convertible_v< t2, ptr_type>>>
	storaging(storaging<t2> ptr_a)
		: ptr(ptr_a.ptr)
	{
	}

	template<class t2, class = enable_if_t<is_convertible_v<ptr_type, t2>>>
	this_type & operator =(t2 ptr_a)
	{
		if (!ptr)
			throw "pointer cannot be empty";
		ptr = ptr_a;
		return *this;
	}
	template<class t2, class = enable_if_t<is_convertible_v<ptr_type, t2>>>
	this_type & operator =(storaging<t2> ptr_a)
	{
		ptr = ptr_a.ptr;
		return *this;
	}



	t & get()const
	{
		return *ptr.get();
	}

	t* operator->()const
	{
		return ptr.get();
	}
	t & operator*()const
	{
		return get();
	}
	ptr_type raw()const
	{
		return ptr;
	}
	operator ptr_type()const
	{
		return ptr;
	}

	template<class stored_type, class ... args_v>
	void reset(args_v && ... args_av)
	{
		ptr = make_shared<stored_type>(std::forward<args_v>(args_av) ...);
	}
private:
	ptr_type ptr;
};

template<class t>
using store_holder_ptr = storaging<holder_ptr<t>>;

template<class t>
struct storaging<shared_ptr<t>>
{
	using ptr_type = shared_ptr<t>;
	template<class t2,class  = enable_if_t<is_convertible_v<shared_ptr<t2>, shared_ptr<t>>>>
	storaging(shared_ptr<t2> with_content)
	{
		if (!with_content)
			throw "pointer cannot be empty";

		ptr = with_content;
	}
	template<class t2, class = enable_if_t<is_convertible_v<shared_ptr<t2>, shared_ptr<t>>>>
	storaging(storaging<shared_ptr<t2>> with_content)
	{
		if (!with_content)
			throw "pointer cannot be empty";

		ptr = with_content.shared();
	}
	template<class stored_type, class ... args_v>
	storaging(Overtype<stored_type>, args_v && ... args_av)
	{
		ptr = make_shared<stored_type>(std::forward<args_v>(args_av) ...);
	}

	template<class t2, class = enable_if_t<is_convertible_v<shared_ptr<t2>, shared_ptr<t>>>>
	auto & operator =(shared_ptr<t2> with_content)
	{
		if (!with_content)
			throw "pointer cannot be empty";

		ptr = with_content;
		return *this;
	}

	t & get()const
	{
		return *ptr.get();
	}
	ptr_type shared()const
	{
		return ptr;
	}
	t* operator->()const
	{
		return ptr.get();
	}
	t & operator*()const
	{
		return get();
	}
	bool operator !()const
	{
		return !ptr;
	}

	auto use_count()const
	{
		return ptr.use_count();
	}

	template<class stored_type, class ... args_v>
	void reset(args_v && ... args_av)
	{
		ptr = make_shared<stored_type>(std::forward<args_v>(args_av) ...);
	}
private:
	ptr_type ptr;
};

template<class t>
using store_shared_ptr = storaging<shared_ptr<t>>;

template<class t>
struct Shared_maker
{
	shared_ptr<t> element;

	Shared_maker() {}
	Shared_maker(const t & object)
	{
		element.reset(new t(object));
	}
	Shared_maker(shared_ptr< t> object)
	{
		element = object;
	}
	template<class _t, typename = enable_if_t<is_convertible_v<_t, t> || is_convertible_v<_t, shared_ptr<t>> >>
	Shared_maker(const _t & object)
	{
		using convertion_type = conditional_t<is_convertible_v<_t, t>, t, shared_ptr<t>>;
		convertion_type converted = object;
		auto a = Shared_maker(converted);
		element = a.element;
	}
};