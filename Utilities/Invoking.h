#pragma once
#include <functional>
#include <map>
#include <vector>
#include <iostream>

#include "Helpers.h"
#include "types.h"
#include "Loose_List.h"
#include "Algo_Pack.h"

using namespace std;

template<class ... args>
struct Invoker:non_replicable
{
	using this_type = Invoker<args...>;
	LooseList<function<void(args...)>> to_invoke;
	LooseListElment<function<void(args...)>> Add(const function<void(args...)> & function)
	{
		return to_invoke.AddLast(function);
	}
	void invoke(args ... arguments)
	{
		for (auto & ti : to_invoke.SaveIterator())
		{
			ti(arguments...);
		}
	}
	bool empty()
	{
		return to_invoke.single();
	}
	void steal_from(this_type & to_steal)
	{
		to_invoke.AttachListBefore(to_steal.to_invoke);
		to_steal.to_invoke.detach();
	}
};

#pragma region variations_caller

template<class inputs_p, class outputs_p>
struct Variation_callback;

template<class ...inputs_v, class ...outputs_v>
struct Variation_callback<Pack<inputs_v...>, Pack<outputs_v...>>
{
	using key_type = tuple<inputs_v...>;
	map<key_type, Invoker<outputs_v... >> invokers;
	map<key_type, Invoker<outputs_v... >> added;
	inline auto add_callback_rapid(const function<void(outputs_v...)>&f, const inputs_v &... keys)
	{
		return invokers[key_type(keys...)].Add(f);
	}
	inline auto add_callback(const function<void(outputs_v...)>&f, const inputs_v &... keys)
	{
		return added[key_type(keys...)].Add(f);
	}
	using signals_p = Pack<inputs_v..., outputs_v...>;
	using inputs_p = Pack<inputs_v...>;
	using function_type = void(outputs_v...);
	inline void invoke(const inputs_v &... keys, const outputs_v & ... arguments)
	{
		key_type key{ keys... };
		auto search = invokers.find(key);
		if(search != invokers.end())
			search->second.invoke(arguments...);
	}
	inline void heartbeat()
	{
		vector<key_type> keys_to_delete;
		for (auto & invoker_node : invokers)
		{
			if (invoker_node.second.empty())
				keys_to_delete.push_back(invoker_node.first);
		}
		for (auto & key : keys_to_delete)
		{
			invokers.erase(key);
		}
	}
	inline void applay_added()
	{
		for (auto & m : added)
		{
			invokers[m.first].steal_from(m.second);
		}
		added.clear();
	}
};

template<class to_add>
struct p_push_fron_type {
	template<class p>
	using type = p_add<to_add, p>;
};
template<class types_P>
struct Variation_maker;
template<class t1, class ... types_v>
struct Variation_maker<Pack<t1, types_v...>>
{
	using variations_without_t1 = typename Variation_maker<Pack<types_v...>>::type;
	using variations_with_t1 = p_detail<typename p_push_fron_type<t1>::template type, variations_without_t1>;
	using type = p_add< variations_with_t1, variations_without_t1 >;
};
template<>
struct Variation_maker<Pack<>>
{
	using type = Pack<Pack<>>;
};

template<class ... to_inheritance_v>
struct Variations_callbacks_user;
template< class to_inheritance1, class ... to_inheritance_v>
struct Variations_callbacks_user< to_inheritance1, to_inheritance_v...>
	:to_inheritance1, Variations_callbacks_user<to_inheritance_v...>
{
	using to_inheritance1::add_callback;
	using Variations_callbacks_user<to_inheritance_v...>::add_callback;
	using to_inheritance1::add_callback_rapid;
	using Variations_callbacks_user<to_inheritance_v...>::add_callback_rapid;
};
template<class to_inheritance>
struct Variations_callbacks_user<to_inheritance> :to_inheritance
{
	using to_inheritance::add_callback;
	using to_inheritance::add_callback_rapid;
};
template<>
struct Variations_callbacks_user<>
{
};

template<class signals_p, class ... to_inheritance_v >
struct Variations_callbacks_user_inheritance;
template<class ... signals_v, class ... to_inheritance_v >
struct Variations_callbacks_user_inheritance<Pack<signals_v...>, to_inheritance_v...>
	:Variations_callbacks_user<to_inheritance_v...>
{
	using base_types_p = Pack<to_inheritance_v...>;
	using signals_p = Pack<signals_v...>;

	template<class invoded_varation, class  tie_type>
	inline void call_invoke(tie_type &  arg_tie)
	{
		using type_signals_p = typename invoded_varation::signals_p;
		invoded_varation & variation = *this;
		type_signals_p::expand([&](auto ... s)
		{
			variation.invoke(get<p_index<signals_p, type_of<decltype(s)>>>(arg_tie)...);
		}
		);
	}
	inline void invoke(signals_v ... signals_a)
	{
		auto arg_tie = std::tie(signals_a...);
		base_types_p::for_each([&](auto t)
		{
			using type = type_of<decltype(t)>;
			call_invoke<type>(arg_tie);
		});
	}
	void heartbeat()
	{
		(to_inheritance_v::heartbeat(), ...);
	}
	
	void applay_added()
	{
		//(to_inheritance_v::applay_added(), ...);//makes corrupltion
		(static_cast<to_inheritance_v*>(this)->applay_added(), ...);
	}

	template<class t>
	using inputs_of = typename t::inputs_p;
	template<class p>
	struct checking
	{
		template<class t>
		using type = P_matches<p, typename t::inputs_p>;
	};
	template<class ... inputs_v>
	static constexpr bool has_matched_variation()
	{
		using inputs_p_a = Pack<inputs_v...>;
		using selected_p = p_select<typename checking<inputs_p_a>::template type, base_types_p>;
		return selected_p::size > 0;
	}
	template<class ... inputs_v>
	using matching_variation = p_element < 0, p_select<typename checking<Pack<inputs_v...>>::template type, base_types_p>>;
};

template<class inputs_p, class variants_inputs_p_p, class variants_p, class outputs_p>
struct Variations_callbacks_apportionment;
template<class inputs_p, class ... variants_inputs_p_v, class variants_p, class outputs_p>
struct Variations_callbacks_apportionment< inputs_p, Pack<variants_inputs_p_v...>, variants_p, outputs_p>
	:Variations_callbacks_user_inheritance< p_add<p_add<inputs_p, variants_p>, outputs_p>,
	Variation_callback<p_add<inputs_p, variants_inputs_p_v>, p_add< p_detach<variants_p, variants_inputs_p_v>, outputs_p>>...>
{
};
template<class inputs_p, class variations_p, class outputs_p>
struct Variations_callbacks
	:Variations_callbacks_apportionment<inputs_p, type_of<Variation_maker<variations_p>>, variations_p, outputs_p>
{
};



#pragma  endregion