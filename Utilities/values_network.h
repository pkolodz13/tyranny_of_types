#pragma once
#include <functional>

#include <Utilities/types.h>
#include <Utilities/Algo_Pack.h>
#include <Utilities/Debug.h>
#include <Utilities/pointers.h>
#include <Utilities/Numeration.h>
#include <Utilities/List_ke.h>
#include <Utilities/numbered_inheritance.h>


template<class t>
struct provider;

template<class t>
struct receptor
{
	using this_type = receptor<t>;
	template<class t2,class... arguments_v>
	receptor(Overtype<t2>,arguments_v && ... arguments_av)
		:received(make_shared<t>( std::forward<arguments_v>(arguments_av)...))
	{
	}
	receptor(const t &  prov):received(make_shared<t>(prov))
	{
	}
	receptor():received(make_shared<t>())
	{
	}
	receptor(provider<t> &  prov)
		:received(prov.get_observator())
	{
		provider_connection = prov.receptors.AddLast(*this);
	}
	receptor(const this_type & to_copy )
		:received(to_copy.received)
	{
		if(to_copy.provider_connection)
		provider_connection = to_copy.provider_connection->AddLast(*this);
	}
	void set(const t & val)
	{
		if (received.use_count() == 1&& !provider_connection)
		{
			(*const_pointer_cast<t>(received.shared())) = val;
		}
		else
		{
			provider_connection.reset();
			received = make_shared<t>(val);
		}
		proceed();
	}
	virtual void proceed() = 0;
	const t & value()
	{
		return received.get();
	}
private:
	friend provider<t>;
	storaging<shared_ptr<const t>> received;
	LooseListElment<reference_wrapper<receptor<t>>> provider_connection;
};

template<class t>
struct provider
{
	using this_type = provider<t>;
	using type = t;
	template<class ... arguments_v>
	provider(arguments_v && ... arguments_av) :provided(Overtype<t>{}, std::forward<arguments_v>(arguments_av)...)
	{
	}
	provider(const this_type & to_copy)
		:provided(Overtype<t>{}, to_copy.provided.get())
	{
	}
	shared_ptr<const t> get_observator()
	{
		return provided.shared();
	}
	shared_ptr< t> danger_observator()
	{
		return provided.shared();
	}
	void add_receptor(receptor<t> & rec)
	{
		rec.received = get_observator();
		rec.provider_connection = receptors.AddLast(rec);
		rec.proceed();
	}

	const t & value()
	{
		return *provided;
	}
protected:
	void set(const t & new_value)
	{
		if (new_value == provided.get())
			return;
		provided.get() = new_value;
		for (auto & a : receptors.SaveIterator())
		{
			a.get().proceed();
		}
	}
private:
	friend receptor<t>;
	storaging<shared_ptr<t>> provided;
	LooseList<reference_wrapper<receptor<t>>> receptors;
};
template<class t>
struct free_provider:provider<t>
{
	using provider<t>::provider;
	using provider<t>::set;
};
#pragma region multiple_receptors

template<class ... t_v>
struct multiple_receptors_inheriting
	:numbered_inheritance<Pack<receptor<t_v>...>>
{
	using types = Pack<t_v...>;
	multiple_receptors_inheriting(provider<t_v> & ... providers)
		:numbered_inheritance<Pack<receptor<t_v>...>>{ providers ...}
	{}
	template<class t>
	receptor<t> & get_receptor()
	{
		return this->get<receptor<t>>();
	}
	template<class t>
	const receptor<t> & get_receptor()const
	{
		return this->get<receptor<t>>();
	}
	template<size_t i>
	receptor<p_element<i, types>> & get_receptor()
	{
		return this->get<i>();
	}
	template<size_t i>
	const receptor<p_element<i, types>> & get_receptor()const
	{
		return this->get<i>();
	}
};
template<class ... t_v>
struct multiple_providers
	:numbered_inheritance<Pack<provider<t_v>...>>
{
	using types = Pack<t_v...>;
	multiple_providers() = default;

	template<class t>
	provider<t> & get_provider()
	{
		return this->get<provider<t>>();
	}
	template<class t>
	const provider<t> & get_provider()const
	{
		return this->get<provider<t>>();
	}
	template<size_t i>
	provider<p_element<i, types>> & get_provider()
	{
		return this->get<i>();
	}
	template<size_t i>
	const provider<p_element<i, types>> & get_provider()const
	{
		return this->get<i>();
	}
};
#pragma endregion

template<class receptors_p, class providers_p, class = make_index_sequence<receptors_p::size>, class = make_index_sequence<providers_p::size> >
struct computing_cell;

template<class ... receptors_v, class ... providers_v, size_t ... rec_iv, size_t ... pro_iv>
struct computing_cell<Pack<receptors_v...>, Pack<providers_v...>, index_sequence<rec_iv...>, index_sequence<pro_iv...>>
	:multiple_receptors_inheriting<receptors_v...>, multiple_providers<providers_v...>
{
	using base_type = multiple_receptors_inheriting<receptors_v...>;
	using providers_p = Pack<providers_v...>;
	using computation_func = std::function<void(const receptors_v &..., providers_v & ...)>;
	computing_cell(computation_func func, provider<receptors_v> & ... providers)
		:base_type(providers...), func(func)
	{
		if (!func)
			throw "there must be function";
		proceed();
	}
	void proceed() override
	{
		tuple<providers_v ...> targets;
		func(this->get_receptor<rec_iv>().value()..., std::get<pro_iv>(targets)...);
		((this->inherited_on_number<provider<providers_v>, pro_iv>::set(std::get<pro_iv>(targets))), ...);
	}

private:
	computation_func func;
};


template<class t>
struct usage_deductor
{
	tuple<> get_providers(const t & prov)
	{
		return { };
	}
};
template<class t>
struct usage_deductor<provider<t>>
{
	using providers_types = t;
	static Numeration<add_pointer_t<provider<t>>> get_providers(provider<t>& prov)
	{
		return Numeration<add_pointer_t<provider<t>>>( &prov );
	}
};
template<class t>
struct usage_deductor<free_provider<t>>
{
	using providers_types = t;
	static Numeration<add_pointer_t<provider<t>>> get_providers(free_provider<t>& prov)
	{
		return Numeration<add_pointer_t<provider<t>>>(&prov);
	}
};
template<class ... rec_v,class ...pro_v,class rep_i,size_t ... pro_iv>
struct usage_deductor<computing_cell<Pack<rec_v...>,Pack<pro_v...>, rep_i,index_sequence<pro_iv...>>>
{
	using providers_types = Pack<pro_v...>;
	static Numeration<add_pointer_t<provider<pro_v>>...> get_providers(computing_cell<Pack<rec_v...>, Pack<pro_v...>>& cell)
	{
		return { &cell.get_provider<pro_iv>()... };
	}
};
packalize_member(receptors_types);
packalize_member(providers_types);
template<class t>
using decay_provider_ptr = std::add_pointer_t< provider< decay_t<t>>>;
template<class t>
struct conversion_checking
{
	template<class t2>
	using type = is_convertible<t2, t>;
};

template<class ... acesible_v>
auto form_accesible_to_needed(const Numeration<provider<acesible_v> * ...> & num, Overtype<Pack<>>)
{
	return num;
}
template<class ... acesible_v,class needed1,class ... needed_v>
auto form_accesible_to_needed(const Numeration<provider<acesible_v> * ...> & num, Overtype<Pack< needed1,needed_v...>>)
{
	using acesible_p = Pack<acesible_v...>;
	using convertibles_p = p_select<conversion_checking<needed1>::template type, acesible_p>;

	static_assert(convertibles_p::size>0,"cannot find conversions");
	using sel = p_element<0, convertibles_p>;
	constexpr size_t sel_num = p_index<acesible_p, sel>;

	return add_to_numeration(num.get<sel_num>(), 
		form_accesible_to_needed(remove_from_numeration<sel_num>(num), Overtype<Pack< needed_v...>>{}));
}


template<class ... parameters_v, class lam_expression>
auto make_computing_cell(lam_expression && func,parameters_v && ... providers_receptors_cells)
{
	using decay_params_p = p_detail< std::decay_t , Pack<parameters_v...>>;
	using accesible_providers_p  = p_collapse < p_detail<packalize_providers_types, decay_params_p>>;

	function deducted_func{ func };
	using function_arguments = typename function_prametrization<decltype(deducted_func)>::arguments_types;

	using raw_receptors_p = p_select<is_non_const_lvalue_refeerence, function_arguments>;
	using needed_raw_providers_p = p_detach< function_arguments, raw_receptors_p>;

	auto num_num_ref = make_numeration(usage_deductor<decay_t<parameters_v>>::get_providers(providers_receptors_cells)...);
	auto collapsed_num = n_Collapse(num_num_ref);
	using expected_providers_p =  p_detail<decay_t, needed_raw_providers_p>;

	auto destilated = form_accesible_to_needed(collapsed_num, Overtype<expected_providers_p>{});

	using receptors_p = p_detail <type_of ,p_detail<remove_pointer_t ,p_from<decltype(destilated)>>>;
	using providers_p =  p_detail<decay_t, raw_receptors_p>;

	using result_type = computing_cell<receptors_p, providers_p>;
	return destilated.expand([&](auto && ... destilat) { return result_type(func,*destilat...);  });

}