#pragma once
#include <memory>
#include "Algo_Pack.h"

struct Activatable
{
	struct Activation
	{
	};
	bool is_active()
	{
		return !activation.expired();
	}
	std::shared_ptr<Activation> activate()
	{
		if (activation.expired())
		{
			std::shared_ptr<Activation> result(new Activation);
			activation = result;
			return result;
		}
		else
		{
			return activation.lock();
		}
	}
	


private :
	std::weak_ptr<Activation> activation;
};

template<class t>
struct State_machine:t
{
	std::shared_ptr<State_machine<t>> & state;
	State_machine(const t & driver, std::shared_ptr<State_machine<t>> & state)
		:t(driver),state(state)
	{
	}
};
